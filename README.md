# C++ String Utilities Library (strutils)

String utilities is meant to give easy access to various functions that C++ is either missing, or are somewhat confusing to use.
Very basic compared to what the boost library has.

# About
The aim of this project is to make basic string modification functions accessible and easy to use.
This header adds various functions that you can find in other languages such as ``trim()``, which don't directly exist in C++.
# How to use
Download ``strutils.h`` and ``strutils.cpp`` and include the header in your program
```c++
#include "strutils.h"
```
To access any of the functions, you have to use ``strutils::`` and the function name
## Example
```c++
#include <iostream>
#include "strutils.h"

int main()
{
	std::string hello = "  Hello";
	hello = strutils::trim(hello);
	std::cout << "The text of the variable 'hello' is " << hello;
}
```
``output: The value of the variable 'hello' is Hello``
# Compiling
To compile your program, you have to run 

``g++ strutils.cpp 'your_program_name'.cpp``

And then run the output file.

# Available functions
```c++
// All of the below use namespace std
vector<string> split(const string text, const char character); // Splits a string based on a character

vector<string> split_once(const string text, const char character); // Splits a string once, based on a character

vector<int> find_all_chars(const string text, const char to_find); // Find every position of a specified character

string trim(const string text) // Trim first spaces off a string until a character is found

string trim_ending(const string text) // Trim spaces after the last character of a string

string trim_trailing(const string text) // Trim both start, and ending spaces

string trim_all(const string text) // Remove every space

string remove_all(const string text, const char/string to_remove); // Remove all of the occurances of a char or string

string replace_all(const string text, const char/string character_to_be_replaced, const char/string character_to_replace) // Replace all instances of a char/string to a different char/string

string replace_char_at(string text, const int position, const char character_to_replace) // Replace a char at a position with a char/string

string remove_char_at(string text, const int location) // Remove a char at a specified location

string to_lowercase(const string text) // Turn string letters into lowercase ones
string to_uppercase(const string text) // Turn string letters into uppercase ones
```
