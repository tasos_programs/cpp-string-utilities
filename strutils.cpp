/* 
String utilities for easy string manipulation.
by oni (aka Tasos Nikolaou).
https://gitlab.com/tasos_programs/cpp-string-utilities/
*/

#include <iostream>
#include <vector>
#include <bits/stdc++.h>
#include "strutils.h"

namespace strutils
{
    // Too many functions i wrote rely on std:: so this makes the code look cleaner
    using namespace std;

    // Function that splits a string based on a character.
    vector<string> split(const string text, const char character)
    {
        vector<string> result; // will hold the split string
        string result_builder; // will be the temp string that will be pushed into our vector
        result_builder.reserve(text.length());

        for (const char letter : text)
        {
            // check if letter is the char we want. While it isn't, insert the char into our temp string
            if (letter != character)
            {
                result_builder.push_back(letter);
                continue;
            }
            // if it is, then insert the string into the vector
            result.push_back(result_builder);
            result_builder = "";
        }
        result.push_back(result_builder); // the last string never gets pushed back from the for loop
        return result;
    }

    // Split a string once
    vector<string> split_once(const string text, const char character)
    {
        vector<string> result;
        int i = 0;

        // loop from the start. When we find the char, we stop.
        while (text[i] != character && i != text.length())
            i++;

        result.push_back(text.substr(0, i));
        try // In case we try to split a string that doesn't have the char inside
        {
            result.push_back(text.substr(i + 1));
        }
        catch (const std::out_of_range &e)
        {
            return result;
        }

        return result;
    }

    string split_and_return_single(string text, const char char_to_split, int position)
    {
        int split_times = -1; // since index 0 is only there when we split the first time

        vector<string> result; // will hold the split string
        string result_builder; // will be the temp string that will be pushed into our vector
        result_builder.reserve(text.length());

        for (const char letter : text)
        {
            if (split_times == position && position != -1)
            {
                return result[position];
            }
            // check if letter is the char we want. While it isn't, insert the char into our temp string
            if (letter != char_to_split)
            {
                result_builder.push_back(letter);
                continue;
            }
            // if it is, then insert the string into the vector
            result.push_back(result_builder);
            result_builder = "";
            split_times ++;
        }
        result.push_back(result_builder); // the last string never gets pushed back from the for loop
        if (position == -1)
            position = result.size() - 1;

        if (result.size() < position)
            throw "Position not found";

        return result[position];
    }

    // Find every position of a specified character
    vector<int> find_all_chars(const string text, const char to_find)
    {
        int i = 0;
        vector<int> result;

        for (const char letter : text)
        {
            if (letter == to_find)
                result.push_back(i);
            i++;
        }
        return result;
    }

    // Trim first spaces of a string
    string trim(const string text)
    {
        int i = 0;
        while (text[i] == ' ' or text[i] == '\t')
            i++;
        return text.substr(i);
    }

    void trim_ref(string &text)
    {
        text = trim(text);
    }

    // Trim ending spaces
    string trim_ending(const string text)
    {
        string result = "";
        int i = text.length() - 1;
        while (text[i] == ' ') // start from the end of the string. when a char is found, stop.
            i--;

        result = text.substr(0, i + 1);
        return result;
    }

    // Trim trailing (beginning + ending) spaces
    string trim_trailing(const string text)
    {
        string result = trim(text);
        result = trim_ending(result);
        return result;
    }

    // Replace a char at a position with char
    string replace_char_at(string text, const int position, const char character_to_replace)
    {
        text[position] = character_to_replace;
        return text;
    }

    // Replace a char at a position with string
    string replace_char_at(string text, const int position, const string string_to_replace)
    {
        // basically an easier to use string.replace method
        text.replace(position, 1, string_to_replace);
        return text;
    }

    // Replace all instances of a char to a different string
    string replace_all(const string text, const char character_to_be_replaced, const char character_to_replace)
    {
        string result;
        result.reserve(text.length());

        for (char letter : text)
            result.push_back(letter == character_to_be_replaced ? character_to_replace : letter);
        return result;
    }

    // Replace all instances of a char to a different char
    string replace_all(const string text, const char character_to_be_replaced, const string character_to_replace)
    {
        string result;

        for (char letter : text)
            result += letter == character_to_be_replaced ? character_to_replace : string(1, letter);
        return result;
    }

    // Replace all instances of a string to a different string
    string replace_all(string text, const string string_to_be_replaced, const string character_to_replace)
    {
        // Get the first occurrence
        int pos = text.find(string_to_be_replaced);

        while (pos != string::npos) // while not string null pos
        {
            text.replace(pos, string_to_be_replaced.size(), character_to_replace);
            // Get the next occurrence
            pos = text.find(string_to_be_replaced, pos + character_to_replace.size());
        }
        return text;
    }

    // Replace all instances of a string to a different char
    string replace_all(string text, const string string_to_be_replaced, const char character_to_replace)
    {
        // Get the first occurrence
        int pos = text.find(string_to_be_replaced);

        while (pos != string::npos) // while not string null pos
        {
            text.replace(pos, string_to_be_replaced.size(), string(1, character_to_replace));
            // Get the next occurrence
            pos = text.find(string_to_be_replaced, pos + 1);
        }
        return text;
    }

    // Remove all specified chars from a string
    string remove_all(const string text, const char character)
    {
        string result;
        result.reserve(text.length()); // Memory optimization

        for (char letter : text)
        {
            // loop while adding every char to a string. When we find the char we don't want, we ignore.
            if (letter == character)
                continue;
            result.push_back(letter);
        }
        return result;
    }

    // Remove all specified strings from a string
    string remove_all(const string text, const string to_remove)
    {
        const string result = replace_all(text, to_remove, "");
        return result;
    }

    // Remove a char at a specified location
    string remove_char_at(string text, const int location)
    {
        // pretty much a rename of the erase method.
        text.erase(location, 1);
        return text;
    }

    // Trim all spaces
    string trim_all(const string text)
    {
        // easier to just use remove_all
        return remove_all(text, ' ');
    }

    // To lower case string
    string to_lowercase(const string text)
    {
        string result;
        result.reserve(text.length());

        for (char letter : text)
        {
            result.push_back(tolower(letter));
        }

        return result;
    }

    // To upper case string
    string to_uppercase(const string text)
    {
        string result;
        result.reserve(text.length());

        for (char letter : text)
        {
            result.push_back(toupper(letter));
        }

        return result;
    }

    // Sort a string
    void sorted(string &text)
    {
        sort(text.begin(), text.end());
    }

    // Check if string in a vector
    bool in(string text, vector<string> list_of_items)
    {
        for (string item : list_of_items)
        {
            if (text == item)
                return true;
        }
        return false;
    }

    // Check if char in a string
    bool in(const string text, const char c)
    {
        for (const char text_char : text)
        {
            if (text_char == c)
                return true;
        }
        return false;
    }

    // Check if int in a vector
    bool in(int number, vector<int> list_of_items)
    {
        for (int item : list_of_items)
        {
            if (number == item)
                return true;
        }
        return false;
    }
};
