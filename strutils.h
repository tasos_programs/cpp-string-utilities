/* 
String utilities for easy string manipulation.
Header file
by oni (aka Tasos Nikolaou).
https://gitlab.com/tasos_programs/cpp-string-utilities/
*/

#include <vector>
#include <string>

namespace strutils
{
    std::vector<std::string> split(const std::string text, const char character); // Splits a std::string based on a character

    std::vector<std::string> split_once(const std::string text, const char character); // Splits an std::string once, based on a character

    std::string split_and_return_single(std::string text, const char char_to_split, int position); // Split and return only the specified index

    std::vector<int> find_all_chars(const std::string text, const char to_find); // Find every position of a specified character

    std::string trim(const std::string text); // Trim first spaces off a std::string until a character is found
    void trim_ref(std::string& text); // Trim string by reference

    std::string trim_ending(const std::string text); // Trim spaces after the last character of a std::string

    std::string trim_trailing(const std::string text); // Trim both start, and ending spaces

    std::string trim_all(const std::string text); // Remove every space

    std::string remove_all(const std::string text, const std::string to_remove);
    std::string remove_all(const std::string text, const char character); // Remove all of the occurances of a char or std::string

    std::string replace_all(const std::string text, const char character_to_be_replaced, const std::string character_to_replace);
    std::string replace_all(std::string text, const std::string string_to_be_replaced, const std::string character_to_replace);
    std::string replace_all(std::string text, const std::string string_to_be_replaced, const char character_to_replace);
    std::string replace_all(const std::string text, const char character_to_be_replaced, const char character_to_replace); // Replace all instances of a char/std::string to a different char/std::string

    std::string replace_char_at(std::string text, const int position, const char character_to_replace); // Replace a char at a position with a char/std::string
    std::string replace_char_at(std::string text, const int position, const std::string string_to_replace);

    std::string remove_char_at(std::string text, const int location); // Remove a char at a specified location

    std::string to_lowercase(const std::string text); // Turn std::string letters into lowercase ones

    std::string to_uppercase(const std::string text); // Turn std::string letters into uppercase ones

    void sorted(std::string &text); // Sorts a string

    bool in(std::string text, std::vector<std::string> list_of_items); // Check if a vector contains a string
    bool in(const std::string text, const char c);                     // find if a string contains a char
    bool in(int number, std::vector<int> list_of_items);               // Check if a vector contains a number
}
