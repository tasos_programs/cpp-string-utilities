#include <cassert>
#include <iostream>
#include "strutils.h"
#define assertm(exp, msg) assert(((void)msg, exp))

using namespace std;

const string text = "Hello there! how are you?";

void test_split()
{
    const vector<string> test = strutils::split(text, ' ');
    const vector<string> test_pass = {"Hello", "there!", "how", "are", "you?"};
    assertm(test == test_pass, "[split] vector not correct");
}

void test_split_once()
{
    vector<string> test = strutils::split_once(text, ' ');
    vector<string> test_pass = {"Hello", "there! how are you?"};
    assertm(test == test_pass, "[split_once] vector not correct");
}

void test_find_all_chars()
{
    vector<int> test = strutils::find_all_chars(text, 'l');
    vector<int> test_pass = {2, 3};
    assertm(test == test_pass, "[find_all_chars] vector not correct");
}

void test_trim()
{
    string test = " Hello";
    test = strutils::trim(test);
    assertm(test == "Hello", "[trim] string not trimmed properly");
}

void test_trim_ending()
{
    string text = "Hello  ";
    text = strutils::trim_ending(text);
    assertm(text == "Hello", "[trim] string not trimmed properly");
}

void test_trim_trailing()
{
    string text = "  Hello There! ";
    text = strutils::trim_trailing(text);
    assertm(text == "Hello There!", "[trim_trailing] string not trimmed properly");
}

void test_remove_all()
{
    string test = strutils::remove_all(text, 'o');
    assertm(test == "Hell there! hw are yu?", "[remove_all] char not removed properly");
    test = strutils::remove_all(text, "ll");
    assertm(test == "Heo there! how are you?", "[remove_all] char not removed properly");
}

void test_trim_all()
{
    const string text = " Hello there! ";
    const string test = strutils::trim_all(text);
    assertm(test == "Hellothere!", "[test_trim_all] string not trimmed correctly");
}

void test_replace_char_at()
{
    string test = strutils::replace_char_at(text, 1, '!');
    assertm(test == "H!llo there! how are you?", "[replace_char_at] char not replaced correctly");
    test = strutils::replace_char_at(text, 1, "!!");
    assertm(test == "H!!llo there! how are you?", "[replace_char_at] char not replaced correctly");
}

void test_replace_all()
{
    string test = strutils::replace_all(text, 'o', "aa");
    assertm(test == "Hellaa there! haaw are yaau?", "[replace_all] chars not replaced");
    test = strutils::replace_all(text, 'o', 'a');
    assertm(test == "Hella there! haw are yau?", "[replace_all] chars not replaced");
    string a = "how this how that";
    test = strutils::replace_all(a, "how", "a");
    assertm(test == "a this a that", "[replace_all] chars not replaced");
    a = "how this how that";
    test = strutils::replace_all(a, 'o', "OO");
    assertm(test == "hOOw this hOOw that", "[replace_all] chars not replaced");
}

void test_remove_char_at()
{
    const string test = strutils::remove_char_at(text, 1);
    assertm(test == "Hllo there! how are you?", "[remove_char_at] char not removed");
}

void test_to_lowercase()
{
    const string test = strutils::to_lowercase(text);
    assertm(test == "hello there! how are you?", "[to_lowercase] string not lowercase");
}

void test_to_uppercase()
{
    const string test = strutils::to_uppercase(text);
    assertm(test == "HELLO THERE! HOW ARE YOU?", "[to_uppercase] string not uppercase");
}

int main()
{
    test_split();
    test_split_once();
    test_find_all_chars();
    test_trim();
    test_trim_ending();
    test_trim_trailing();
    test_trim_all();
    test_remove_all();
    test_replace_all();
    test_replace_char_at();
    test_remove_char_at();
    test_to_lowercase();
    test_to_uppercase();
    cout << "All tests passed!" << endl;
}
